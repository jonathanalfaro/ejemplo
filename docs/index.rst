Bienvenido a la documentación del programa 'Ejemplo'
====================================================
Este programa 'Ejemplo' es un programa que solo sirve de ejemplo

Sección 1:

.. toctree::
   :maxdepth: 2
   :caption: Esta es la sección uno

   pagina1.md
   pagina2.md
   
Seccion 2:

.. toctree::
  :maxdepth: 2
  :caption: Esta es la sección dos

  pagina3.md
  pagina4.md
